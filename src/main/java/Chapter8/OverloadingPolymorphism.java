package Chapter8;

//ad-hoc/overloading

/***
 * This is an example of overloading/ad-hoc polymorphism.
 * In this class, methods of the same identifier are
 * overloaded to be able to accept parameters of different
 * types. Method multiply can accept two ints or it can accept
 * two doubles.
 */
public class OverloadingPolymorphism {

    //Method multiply is overloaded with different signatures.
    //This method accepts ints
    public int Multiply(int x, int y){
        return x * y;
    }
    //This method accepts doubles;
    public double Multiply(double x, double y){
        return x * y;
    }


    public static void run() {
        OverloadingPolymorphism m = new OverloadingPolymorphism();
        m.Multiply(11,9);
        m.Multiply(3.5,2.5);
    }
}
