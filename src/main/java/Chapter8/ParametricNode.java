package Chapter8;

/**
 * This is an example of polymeric polymorphism.
 * This class a node class similar to a node of linked
 * list. The a type of the node parameter is generic
 * and can be defined at run time.
 * Generic type is T. A class can is not limited
 * to one generic type. [8.2]
 * This class is public and accessible to any scope [6.6]
 */

public class ParametricNode <T>{
    T value;
    ParametricNode<T> nextNode;

    public ParametricNode(T value){
        this.value = value;
        this.nextNode = null;
    }

    public T getValue(){
        return this.value;
    }

    public void setValue(T value){
        this.value = value;
    }
    public void setNextNode(ParametricNode<T> node){
        this.nextNode = node;
    }

    ParametricNode<T> getNextNode(){
        return this.getNextNode();
    }



    public static void run(){
        ParametricNode<Integer> node = new ParametricNode<>(1);
        ParametricNode<Integer> node2 = new ParametricNode<>(2);
        node.setNextNode(node2);
    }

}
