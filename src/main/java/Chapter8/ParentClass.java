package Chapter8;

/**
 * Here is an implementation of an abstract class. The class is can not
 * be instantiated. Method must be abstract if not defined, or they may
 * be final if defined. This abstract class the super class of ParentClass
 * bellow. This is an example of subtype polymorphism.
 * This class is package protected and accessible to only this package [6.6]
 */
abstract class AbstractClass {

    Object object;

    /**
     * Abstract method. Must be overridden in sub classes. [8.4.3.1]
     */
    abstract void method01();

    /**
     * Final Method. Cannot be overridden in sub classes [8.4.3.3]
     */
    final void finalMethod(){
        System.out.println("I am a final method");
    }

}

public class ParentClass extends AbstractClass {
    /**
     * Here are a classes fields. These are not initialized.
     */
    public Object publicField;
    public final Object finalField;
    private Object privateField;
    Object packageField;
    int i;

    /**
     * Instance Initializer.
     */
    Object initializedPackage = new Object();

    /**
     * Static fields. There exist only one instance of this field
     * no matter how many instances of classes exist. The field is
     * only initialized onec. [8.3.1.1]
     */
    static int objectCounter = 0;


    /**
     * Package level constructor [8.8.3]
     */
    ParentClass(){
        finalField = new Object();
        publicField = new Object();
        privateField = new Object();
        packageField = new Object();
        objectCounter++;
        {
            int x = 10;
            int y = 100;
            i = x*y;
        }
    }

    /**
     * Overloading the constructor [8.8.8]
     */
    ParentClass(int count){
        this.objectCounter = count;
        finalField = new Object();
    }


    /**
     * Static method can be invoked without an instance of its parent. [8.4.3.2]
     */
    static void method02(){
        System.out.println("Static Method");
    }

    //Overriding abstract method from parent class

    /**
     * Overriding the parent's method [8.4.8.1].
     */
    @Override
    void method01() {
        System.out.println("I am an overridden method calling a final method.");
        finalMethod();
    }

    /**
     * Overloading method [8.4.9.]
     */
    void overloadedMethod(int x){
        System.out.println(x);
    }
    /**
     * Overloading method [8.4.9.]
     */
    void overloadedMethod(double x) {
        System.out.println(x);
    }

    /**
     * Private class that is inner class. Only accessible to its Parent.
     * Only can be created if instantiated by parent. [8.1.3] [6.6]
     *
     */
    private class PrivateInnerClass{
        public int method02(){
            return 2;
        }
    }

    /**
     * Public class that is inner class.
     * Only can be created if instantiated by parent. [8.1.3]
     */
    public class PublicInnerClass{
        public int method03(){
            return 3;
        }
    }

    /**
     * Package access class. Class is a nested class and extends the outer class.[8.1.4],[8.1.3]
     */
    class InnerExtendedClass extends ParentClass{
        public int method04(){
            return 4;
        }
    }


    /**
     * Nested static class. Can be instantiated without an instance of its parent.[8.1.3]
     */
    static class InnerStaticClass{
        public int method05(){
            return 5;
        }
    }

    public static void run() {

        /**
         * Creating parent object with default constructor.
         */
        ParentClass parentClass = new ParentClass();
        /**
         * Creating parent object with overloaded constructor.
         */
        ParentClass parentClass02 = new ParentClass(parentClass.objectCounter);

        /**
         * Creating private inner class with instance of parent object.
         */
        PrivateInnerClass privateInnerClass = parentClass.new PrivateInnerClass();

        /**
         * Creating public inner class with instance of parent object.
         */
        PublicInnerClass publicInnerClass = parentClass.new PublicInnerClass();

        /**
         *Calling inner class with instance of the parent class.
         */
        InnerExtendedClass innerExtendedClass = parentClass.new InnerExtendedClass();

        /**
         * Calling static inner class without instance of the parent class.
         */
        ParentClass.InnerStaticClass x = new InnerStaticClass();

        /**
         * Calling ParentClass static method without an instance of the object.
         */
        ParentClass.method02();

        /**
         * The parent is the reference type, the object is a subclass
         */
        AbstractClass dynamicDispatch = new ParentClass();
        dynamicDispatch.finalMethod();
    }
}

/**
 * Private constructor so instantiation is prevented [8.8.10]
 */
class InstantiationPrevention{
    private InstantiationPrevention(){};
    static int var = 1_000_000;
}


