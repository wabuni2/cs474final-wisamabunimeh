package Chapter15;

public class Chapter15 {

    public static void run() {

    //Many of the concepts from chapter 15 are demonstrated throughout the project.
        /**
         * Results of an expression [15.1]
         */
        {
            //Expression that results in a value
            int val = 1 + 2;

            //Expression that results in a variable
            int val2 = val;
        }

        /**
            Java's different forms of expressions [15.21]
         */
        {
            //Expression name
            int val;

            //Primary expression
            int val01 = 3;

            //Unary expression
            val01++;

            //Binary operators
            boolean a = false;
            boolean b = true;
            boolean c = a^b;

            //Ternary Operator
            int val03 = a?1:0;

        }

        /**
         * Type of Expression
         * The type is known at compile time if the variable
         * or value is denoted. [15.3]
         */
        {
            String s  = "my type is known at compile time;";
        }
    }
}
