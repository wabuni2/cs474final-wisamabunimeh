package Chapter14;

import java.util.HashSet;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class BlocksAndStatements {

    /**
     *  Static Initializers [8.7]
     */
    static String wisam = "Wisam";
    static{
        wisam = "masiW";
        System.out.println(wisam);
    }

    /**
     * Method implemented using continue, for loop and
     * an if statement [14.1], [14.14], [14.9]
     */
    static void continueExample(){
        for(int i = 0; i < 10; i++){
            if(i%2 == 0){
                System.out.println("Even: " + i);
                continue;
            }
            System.out.println("Odd: " + i);
        }
    }

    /**
     * Implementing method using break, if-then
     *  and if-else. [14.9.1] [14.9.2] and 14.1
     */
    static void breakExample(){
        for(int i = 1; i < 10; i++){
            if(i%4 == (0)){
                break;
            }
            else if(i%3==0){
                System.out.println("dividable by 3: " + i);
                continue;
            }
            else{
                System.out.println("not dividable by 4: " + i);
            }
            System.out.println("~*~*~*~*~*~*~");

        }
    }

    /**
     * Implementing method uses return
     * statement with a value
     * [14.1]
     */
    static int returnWithValueExample(){
        int randomInt = ThreadLocalRandom.current().nextInt(0, 1000);
        System.out.println("Random integer: " + randomInt);
        return randomInt;
    }


    /**
     * Implementing method uses return
     * statement with no value
     * [14.1]
     */
    static void returnWithNoValueExample(){
        int randomInt = ThreadLocalRandom.current().nextInt(0, 1000);
        System.out.println("Random integer: " + randomInt);
        return;
    }

    /**
     * Method with an inner scope. [14.2]
     */
    static void staticBlockExample(){

        int x = 100;
        int y = 40;

        {
            int z = 55;
            int q = -44;
            x = x + z + q;
        }
        System.out.println("x: " + x + "y: " + y);
    }

    /**
     * Method with that gets a user input on direction using
     * the WASD keys on the keyboard.
     * This method uses a switch statment with a fall through,
     *  and a while loop. [14.11.1], [14.12], [16.1.6] [15.28]
     */
    static Direction getDirection(){
        Scanner myObj = new Scanner(System.in);  // Create a Scanner object
        while(true) {
            System.out.println("Enter direction (w, a, s, d)");
            String direction = myObj.nextLine();  // Read user input
            direction.toLowerCase();
            switch (direction) {
                case "w":
                    return Direction.UP;
                case "a":
                    return Direction.LEFT;
                case "s":
                    return Direction.DOWN;
                case "d":
                    return Direction.RIGHT;
            }
        }
    }

    /**
     * enum declaration for direction types.
     * [8.9]
     */
    enum Direction {
        LEFT,
        RIGHT,
        UP,
        DOWN
    }

    /**
     * The do statement [14.13]
     */
    static void doWhileExample(){

        int x = 0;
        do x++;
        while(x < 10);
        System.out.println("DoWhile example: " + x);


    }

    /**
     * The enhanced python-like for statement [14.14.1]
     */
    static void enhancedForLoop(){
        HashSet<Integer> set = new HashSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        for(int num : set){
            System.out.println(num);
        }

    }

    /**
     * Try catch and try finally statements
     * [14.20], [14.20.2]
     */
    static void tryCatchExample(){
        ReentrantLock l = new ReentrantLock();
        Condition condition = l.newCondition();
        boolean x = true;
        l.lock();
        try {
            try {
                condition.await();
            }catch (InterruptedException e) {
                e.printStackTrace();
            }
         }
         finally {
            l.unlock();
        }
    }

    /**
     * Try catch finally
     * [14.20.2]
     */
    static void TryCatchFinally() {
        try {
            System.out.println("try");
        }
        catch (Exception e){
           e.printStackTrace();
        }
        finally {
            System.out.println("Finally");
        }

    }

    /**
     * Throws example [14.18]
     */
    static void throwsExample() throws Exception{
        System.out.println("Exception");
    }

    public static void run() {

        continueExample();
        breakExample();
        int randomVal = returnWithValueExample();
        returnWithNoValueExample();
        doWhileExample();
        staticBlockExample();
        getDirection();
        enhancedForLoop();
        tryCatchExample();
        TryCatchFinally();

        try {
            throwsExample();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


}
