package FunctionalProgramming;
import java.util.ArrayList;
import java.util.function.Function;

public class FunctionalProgramming {


    public static void run() {

        {
            /**
             * Implementing a lambda functions that return lambda functions
             * that your code will invoke at some point later.
             */
            //Here the function is called, and a function is saved into var myFunctionForLater
            Function<Integer, Integer> myFunctionForLater = (((q) -> (q * q)));

            //Here the saved function is now executed
            int functionOut = myFunctionForLater.apply(10);
            System.out.println(functionOut);

        }

        {
            /**
             * Implementing one function that take lists of some other
             * functions and composes them into a function
             * that it returns and will be invoked in your code.
             * [15.27]
             **/

            //Creating lambda functions
            ArrayList<Function<Integer, Integer>> lis = new ArrayList<Function<Integer, Integer>>();
            Function<Integer, Integer> square = (q -> (q * q));
            Function<Integer, Integer> timesTwo = (q -> (q * 2));
            Function<Integer, Integer> increment = (q -> (q + 1));
            Function<Integer, Integer> decrement = (q -> (q - 1));

            //Populating a list of functions
            lis.add(square);
            lis.add(timesTwo);
            lis.add(increment);
            lis.add(decrement);

            //Lambda function that executes each function in the list
            lis.forEach( a -> System.out.println(a.apply(4)));
        }
    }
}
