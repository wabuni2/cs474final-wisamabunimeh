package Chapter16;

public class Chapter16 {


    public static void run() {
        //Many chapter 16 concepts are spread throughout this project.

        /**
         * val is assigned after a constant expression whose value
         * is true [16.1.1]
         */
        {
            int val;
            if(true){
                val = 100;
            }
        }

        /**
         * val is remains unassined after a&b is false
         * [16.1.2]
         */
        {
            int val;
            boolean a = true;
            boolean b = false;
            if(a&&b ){
                val = 100;
            }
        }

        /**
         * val is assigned after a|b is true
         * [16.1.3]
         */
        {
            int val;
            boolean a = true;
            boolean b = false;
            if(a||b ){
                val = 100;
            }
        }

        /**
         * val remains unassigned as !a is false.
         * 16.1.4
         */
        {
            int val;
            boolean a = true;
            if(!a){
                val = 100;
            }
        }


        /**
         * val must be assigned after a decrement or
         * incremnet. If it is not assigned, then a
         * compile time error occurs. [16.1.9]
         */
        {
            int val = 0;
            ++val;
        }

        /**
         * val is unassigned after an empty
         * statement [16.2.1]
         */
        {
            int val;
        }


    }


}
