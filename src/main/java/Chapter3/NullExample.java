package Chapter3;

/**
 * The null literal is a null type which is special
 * because an variable cannot be of null type 3.10.7.
 * A variable can be assigned to null though 4.1.
 * In NullExample.java, this demonstrates the use of
 * the null literal for a singleton design pattern.
 * This pattern checks if a static object is null,
 * and if it is not null, then the static object was
 * already created previously. Otherwise, the object
 * must be created and assigned to that reference.
 * This patters in from the homework and was used
 * to instantiate a logging object.
 */
public class NullExample{
    private static NullExample logger = null;
    public static NullExample getLogger() {
        if (logger == null) {
            logger = new NullExample();
        }
        return logger;
    }

    public static void run(){
        NullExample nullExample = new NullExample();
    }
}