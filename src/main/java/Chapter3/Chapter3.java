package Chapter3;


public class  Chapter3 {
     public Chapter3(){


        /**Some of the Java literals are demonstrated here.
         * Literals are text representation of a Java primitive type [3.10].
         * Included within these literals, besides int, float, and boolean,
         * are escape sequences for strings. Escape sequences area \ and an
         * ASCII character that represent some “non-graphic” character like
         * return, tab, or new line [3.10.6]. The new line escape sequence
         * is demonstrated in Chapter3.java. Operators are also demonstrated
         * throughout the project.
         */

        //Identifier and literal
        final int var01 = 1;

        //Float literal
        float var02 = 99.9f;

        //Boolean literal
        boolean var03 = false;

        //Character literal
        char var04 = '*';

        //String literal
        String var05 = "Java uses Unicode to encode syntax.";

        //Escape Sequences for Character and String Literals
        String var06 = "\n";

        //Operator tokens
        boolean var07 = (1==0);
    }

    public static void run() {
        Chapter3 c3 = new Chapter3();
    }
}



