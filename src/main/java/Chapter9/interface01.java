package Chapter9;

/**
 * Functional interface. Has only one abstract method.[9.8] [9.9]
 */
interface FunctionalInterface{
    void function();
}


/**
 * A Super generic interface [9.1.2]
 */
interface interface03<T> {
    /**
     * Interface body and member declaration [9.1.4][9.2][9.3]
     */
    int i03 = 3_0;
    T method03();
}

/**
* A Super generic interface [9.1.2]. [9.1.3]
*/
interface interface02<S> {
    /**
     * Interface body and member declaration [9.1.4][9.2][9.3]
     */
    int i02 = 2_0;
    void method02(S var);
}

/**
 * A sub interface [9.1.2]. [9.1.3] [9.4.1]
 * No longer generic as extended types are defined.
 */
interface interface01 extends interface02<Integer>, interface03<Float>, FunctionalInterface{
    /**
     * Interface body and member declaration [9.1.4][9.2][9.3]
     */
    int i01 = 1_0;
    void method();

    /**
     * 9.4. Method Declarations and Overriding and overloading method from superinterface. [9.4.1][9.4.2]
     * and interface method body [9.4.2]
     */
    default void method02(int x){
        System.out.println(x);
    };
    /**
     * 9.4. Method Declarations and Overriding and overloading method from superinterface.
     */
    default void method02(String x){
        System.out.println(x);
    };
    //Defining a method within an interface using the default keyword.
    default void defaultMethod(){
        System.out.println(i01);
    }

}

/**
 *A class that implements a sub interface.
 */
class ClassExample implements interface01, FunctionalInterface{

    @Override
    public void function() {
        System.out.println("function");
    }

    @Override
    public Float method03() {
        return .0002f;
    }

    @Override
    public void method02(Integer var) {
        System.out.println("method02");
    }

    @Override
    public void method() {
        System.out.println("method");
    }

}