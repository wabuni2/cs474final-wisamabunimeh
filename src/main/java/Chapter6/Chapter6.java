package Chapter6;


public class Chapter6 {
    int i = 10;

    /**
     * Implementing shadowing a local varible.
     * Class field i is instantiated.
     * Method field i is instantiated in method scope
     * method field is shadowing class field. [6.4.1]
     */
    static void shadowingByLocalVariable() {
        int i = 9;
        System.out.println("shadowed by i = " + i);
    }

    public static void run() {
        shadowingByLocalVariable();
    }
}


