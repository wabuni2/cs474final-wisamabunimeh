package Chapter6;
/**
 * The System and out are Obscured by class System and out.
 * So call to System.out. are calls this class.
 * println is a method of this class.
 * System.out.println will print nothing.
 * Must use the fully qualified name of
 * java.lang.System.out.println("") to use the desired
 * println method. [6.4.2], [6.7]
 *
 */
public class ObscuringExample{
    public static ObscuringExample System, out;
    public static void println(Object s) {}

    public static void run() {
        System.out.println("****~~~~wIsAm AbUnImEh~~~~***");
        java.lang.System.out.println("TRUE OUTPUT!");
    }
}