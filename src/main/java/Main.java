/**
 * Wisam Abunimeh
 * CS 474
 */

import Chapter3.*;
import Chapter15.*;
import Chapter8.*;
import Chapter6.*;
import Chapter14.*;
import FunctionalProgramming.*;


public class Main {

    public static void main(String[] args) {
        Chapter3.run();
        NullExample.run();
        OverloadingPolymorphism.run();
        ParametricNode.run();
        ParentClass.run();
        Chapter6.run();
        BlocksAndStatements.run();
        FunctionalProgramming.run();
        Chapter15.run();
    }
}
